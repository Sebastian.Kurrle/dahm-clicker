
const app = Vue.createApp({
    data() {
        return {
            imgSrc: 'img/dahm.jpeg',
            clicks: 0
        }
    },

    methods: {
        dahmClick() {
            this.clicks++;
            localStorage.setItem('clicks', this.clicks)
            const actionsValues = ['es macht so viel spaß', 'du bist so gut', 'mach weiter so', 'woher kannst du das nur', 'ich bin hin und weg']

            let randomValue = Math.floor(Math.random() * actionsValues.length)
            this.$refs.actionValue.innerText = actionsValues[randomValue]
        }
    },

    created() {
        if (localStorage.getItem('clicks') == undefined) {
            localStorage.setItem('clicks', 0)
        } else {
            this.clicks = localStorage.getItem('clicks')
        }
    }
})

app.mount('#app')
